import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { BehaviorSubject } from 'rxjs';

import { find } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  // API secret key of http://localhost:4004 for darindam507@gmail.com
  private apiKey: string = '5ca993d72cf3c702253c1346';
  private apiSecret: string = 'zdth2azdth13zdthnOLsnETFZ74rHMFiQjy2Bex7AoceiZLbxM0RlnNCl5ob2tplONtkW';

  private apiKeyLive: string = '5ca067f88f272e00173c55a4';
  private apiSecretLive: string = 'zdth2azdth13zdthX13NhrT0JD8X4SzTYBGEKuwnGsNzTBjGZzdonCDzdonTnudwzdoncz/XSZk8lG';

  // Bearer token for darindam507@gmail.com
  private bearerToken: string = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImRhcmluZGFtNTA3QGdtYWlsLmNvbSIsImp3dFZhbGlkYXRlZEF0IjoiMjAxOS0wMy0wMlQyMTo1NTozMS4zMzVaIiwicm9sZSI6MCwiaWF0IjoxNTc2NTU3MDE4LCJleHAiOjE1NzY1NjQyMTh9.07dnpqCUG6nzG8sjqI0-pHsroDzpkKi4iKJHrdzEjQw';

  // Bearer token for subhojit1992.mondal@gmail.com
  // private bearerToken: string = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InN1Ymhvaml0MTk5Mi5tb25kYWxAZ21haWwuY29tIiwiand0VmFsaWRhdGVkQXQiOiIyMDE5LTAzLTA4VDA5OjQ1OjM2LjAyNloiLCJyb2xlIjoxLCJpYXQiOjE1NTUyOTUzNDEsImV4cCI6MTU1NTMwMjU0MX0.kaTB1UbPTZFdnakZXBaF2u8iPWHoYkcXopCelHlJeps';

  public socketInitialized: boolean = false;

  private data: StoreData;
  public subject = new BehaviorSubject(this.data);
  public observable = this.subject.asObservable();

  constructor() {
    this.data = {
      users: [
        { key: 'a', name: 'Abhisek', value: 10 },
        { key: 'b', name: 'Arindam', value: 50 },
        { key: 'c', name: 'Debansu', value: 40 },
        { key: 'd', name: 'Dibya', value: 20 },
        { key: 'e', name: 'Rahul', value: 100 },
        { key: 'f', name: 'Soumen', value: 70 }
      ]
    };

    this.subject.next(this.data);
  }

  updateUser(user: User, newUser: User) {
    Object.assign(find(this.data.users, user), newUser);
    this.subject.next(this.data);
  }

  auth() { return Object.assign({}, this.secret(), this.authorization()); }

  key() { return { api: environment.production ? this.apiKeyLive : this.apiKey }; }

  secret() { return { secret: environment.production ? this.apiSecretLive : this.apiSecret }; }

  authorization() { return { authorization: this.bearerToken }; }
}

export interface User {
  key: string;
  name: string;
  value: number;
}

export interface StoreData {
  users: Array<User>;
}
