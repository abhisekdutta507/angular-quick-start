import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor() { }

  async wait(d: number) {
    return new Promise((success, error) => {
      setTimeout(() => {success(true)}, d * 1000);
    });
  }
}
