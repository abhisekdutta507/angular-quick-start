import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/**
 * @description Socket Modules for Social Chat.
 */
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

import { environment } from '../../../environments/environment';
/**
 * @description domain update with production type.
 */
const apiurl = environment.production ? 'https://zazuconnection.herokuapp.com' : 'http://localhost:5000';
/**
 * @description Select domain available only on Heroku.
 */
// const apiurl = 'https://zazuconnection.herokuapp.com';
const config: SocketIoConfig = { url: apiurl, options: {} };

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SocketIoModule.forRoot(config),
  ],
  exports: [
    /**
     * @description Shared modules are stored here.
     */
    SocketIoModule,
  ]
})
export class SocketModule { }
