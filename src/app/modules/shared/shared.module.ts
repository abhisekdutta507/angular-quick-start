import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { IoListComponent } from '../../components/io-list/io-list.component';
import { IoUnitComponent } from '../../components/io-unit/io-unit.component';

@NgModule({
  declarations: [
    IoListComponent,
    IoUnitComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    IoListComponent,
    IoUnitComponent,

    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
