import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

/* Material sidenav module requirements */
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
/* Material sidenav module requirements */

/* Material form-field module requirements */
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
/* Material form-field module requirements */

import { MatCheckboxModule } from '@angular/material/checkbox';

import { MatCardModule } from '@angular/material/card';

import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    MatButtonModule,

    /* Material sidenav module requirements */
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    /* Material sidenav module requirements */

    /* Material form-field module requirements */
    MatFormFieldModule,
    MatInputModule,
    /* Material form-field module requirements */

    MatCheckboxModule,

    MatCardModule,

    MatDialogModule
  ],
  exports: [
    MatButtonModule,

    /* Material sidenav module requirements */
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    /* Material sidenav module requirements */

    /* Material form-field module requirements */
    MatFormFieldModule,
    MatInputModule,
    /* Material form-field module requirements */

    MatCheckboxModule,

    MatCardModule,

    MatDialogModule
  ]
})
export class MaterialModule { }
