import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './components/landing/landing.module#LandingModule'
  },
  {
    path: 'commands',
    loadChildren: './components/commands/commands.module#CommandsModule'
  },
  {
    path: 'components',
    loadChildren: './components/components/components.module#ComponentsModule'
  },
  {
    path: 'chat',
    loadChildren: './components/chat/chat.module#ChatModule'
  },
  {
    path: 'input-output',
    loadChildren: './components/io-decorators/io-decorators.module#IoDecoratorsModule'
  },
  {
    path: 'popmotion',
    loadChildren: './components/popmotion/popmotion.module#PopmotionModule'
  },
  {
    path: 'behavioursubject',
    loadChildren: './components/behavioursubject/behavioursubject.module#BehavioursubjectModule'
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
