import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../../modules/shared/shared.module';
import { MaterialModule } from '../../modules/material/material.module';
import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat.component';
import { DemoChatComponent } from './demo-chat.component';

import { SocketModule } from '../../modules/socket/socket.module';

@NgModule({
  declarations: [ChatComponent, DemoChatComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule,
    ChatRoutingModule,
    SocketModule,
  ]
})
export class ChatModule { }
