import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';

import { StoreService } from '../../services/store.service';

let that: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  private auth: any;

  constructor(
    public socket: Socket,
    private store: StoreService
  ) {
    that = this;
  }

  ngOnInit() {
    /**
     * @description Initialize the this object into that.
     */
    that = this;

    if(!this.store.socketInitialized) {
      this.socket.on('loggedIn', this.loggedIn);
      this.socket.on('joined', this.joined);
      this.socket.on('error', this.error);
      /**
       * @description socket initilized permanently
       */
      this.store.socketInitialized = true;
    }

    /**
     * @param event event name is to be mentioned
     * @param data { email: 'email id', fullName: 'user\'full name' } or {}
     */
    // this.auth = Object.assign({}, this.store.auth());
    this.socket.emit('login', Object.assign({ data: Object.assign({}, that.store.key()), auth: this.store.auth()}));
  }

  socketStatus(type: string) { console.log(this.socket.ioSocket); return type ? this.socket.ioSocket[type] : this.socket.ioSocket; }

  loggedIn(event: any) {
    console.log(event);

    that.socket.emit('join', { data: Object.assign({}, that.store.key()), auth: that.store.auth()});
  }

  joined(event: any) {
    console.log(event);
  }

  error(event: any) {
    console.log(event);
  }

  chat(event: any) {
    console.log(event);
  }

}
