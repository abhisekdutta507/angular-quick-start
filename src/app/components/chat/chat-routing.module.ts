import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatComponent } from './chat.component';
import { DemoChatComponent } from './demo-chat.component';

const routes: Routes = [
  {
    path: '',
    component: ChatComponent
  },
  {
    path: 'demo',
    component: DemoChatComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
