import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { uniq, pull } from 'lodash';
import { Socket } from 'ngx-socket-io';

import { StoreService } from '../../services/store.service';

let that: any;

@Component({
  selector: 'app-demo-chat',
  templateUrl: './demo-chat.component.html',
  styleUrls: ['./demo-chat.component.scss']
})
export class DemoChatComponent implements OnInit {
  @ViewChild('formElement') formElement: ElementRef;
  
  public form = new FormGroup({
    message: new FormControl({ value: '', disabled: false }, [])
  });

  private chatList: Array<any>;

  constructor(
    public socket: Socket,
    private store: StoreService
  ) {
    this.chatList = [];

    that = this;
  }

  ngOnInit() {
    /**
     * @description Initialize the this object into that.
     */
    that = this;

    if(!this.store.socketInitialized) {
      this.socket.on('loggedIn', this.loggedIn);
      this.socket.on('joined', this.joined);
      this.socket.on('chat', this.chat);
      this.socket.on('error', this.error);
      /**
       * @description socket initilized permanently
       */
      this.store.socketInitialized = true;
    }

    /**
     * @param event event name is to be mentioned
     * @param data { email: 'email id', fullName: 'user\'full name' } or {}
     */
    this.socket.emit('login', Object.assign({ data: {} }));
  }

  socketStatus(type: string) { console.log(this.socket.ioSocket); return type ? this.socket.ioSocket[type] : this.socket.ioSocket; }

  loggedIn(event: any) {
    // console.log('loggedIn', event);
    that.socket.emit('join', { data: {} });
  }

  joined(event: any) {
    // console.log('joined', event);
  }

  error(event: any) {
    console.log(event);
  }

  chat(event: any) {
    that.chatList.push(event.data);
    // console.log(event);
  }

  send(event: Event) {
    const { message } = this.form.controls;
    that.socket.emit('chatRequest', { data: { message: message.value } });
    message.setValue('');
  }

  async clear() {
    this.form.controls.message.setValue('');
  }

}
