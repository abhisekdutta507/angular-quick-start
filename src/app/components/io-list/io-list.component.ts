import { Component, Output, Input, EventEmitter, HostBinding } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';

@Component({
  selector: 'app-io-list',
  templateUrl: './io-list.component.html',
  styleUrls: ['./io-list.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        // height: '40px',
        overflow: 'hidden'
      })),
      state('closed', style({
        height: '0px',
        overflow: 'hidden'
      })),
      transition('open => closed', [
        animate('0.2s')
      ]),
      transition('closed => open', [
        animate('0.2s')
      ]),
    ]),
  ],
})
export class IoListComponent {

  constructor() { }

  ngOnInit() {
  }

  @Input() text: string;

  visible: boolean = false;
  @Output() open: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  @Output() remove: EventEmitter<any> = new EventEmitter();
 
  toggle() {
    this.visible = !this.visible;
    if (this.visible) {
      this.open.emit(null);
    } else {
      this.close.emit(null);
    }
  }

  removeFn(event: Event, text: string) {
    this.remove.emit(text);
  }

}
