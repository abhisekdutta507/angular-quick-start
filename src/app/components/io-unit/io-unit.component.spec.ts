import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoUnitComponent } from './io-unit.component';

describe('IoUnitComponent', () => {
  let component: IoUnitComponent;
  let fixture: ComponentFixture<IoUnitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoUnitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
