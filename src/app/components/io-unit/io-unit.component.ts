import { Component, OnInit, Input } from '@angular/core';

import { User, StoreService } from '../../services/store.service'

@Component({
  selector: 'app-io-unit',
  templateUrl: './io-unit.component.html',
  styleUrls: ['./io-unit.component.scss']
})
export class IoUnitComponent implements OnInit {

  @Input() user: User;

  constructor(
    private store: StoreService
  ) { }

  increase() {
    let nU: User = Object.assign({}, this.user); nU.value += 1;
    this.store.updateUser(this.user, nU);
  }

  decrease() {
    let nU: User = Object.assign({}, this.user); nU.value -= 1;
    this.store.updateUser(this.user, nU);
  }

  ngOnChanges() {
    // console.log('ngOnChanges', this.user.key);
  }

  ngOnInit() {
    // console.log('ngOnInit', this.user.key);
  }

  ngDoCheck() {
    // console.log('ngDoCheck', this.user.key);
  }

  ngAfterContentInit() {
    // console.log('ngAfterContentInit', this.user.key);
  }

  ngAfterContentChecked() {
    // console.log('ngAfterContentChecked', this.user.key);
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit', this.user.key);
  }

  ngAfterViewChecked() {
    // console.log('ngAfterViewChecked', this.user.key);
  }

  ngOnDestroy() {
    // console.log('ngOnDestroy', this.user.key);
  }

}
