import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BehavioursubjectRoutingModule } from './behavioursubject-routing.module';
import { SharedModule } from '../../modules/shared/shared.module';
import { MaterialModule } from '../../modules/material/material.module';

import { BehavioursubjectComponent } from './behavioursubject.component';

@NgModule({
  declarations: [
    BehavioursubjectComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    BehavioursubjectRoutingModule
  ]
})
export class BehavioursubjectModule { }
