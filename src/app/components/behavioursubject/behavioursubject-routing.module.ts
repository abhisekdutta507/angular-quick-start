import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BehavioursubjectComponent } from './behavioursubject.component';

const routes: Routes = [
  {
    path: '',
    component: BehavioursubjectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BehavioursubjectRoutingModule { }
