import { Component, OnInit } from '@angular/core';

import { StoreService, StoreData } from '../../services/store.service';

@Component({
  selector: 'app-behavioursubject',
  templateUrl: './behavioursubject.component.html',
  styleUrls: ['./behavioursubject.component.scss']
})
export class BehavioursubjectComponent implements OnInit {

  public storeData: StoreData;
  public uVS: number;

  constructor(
    private store: StoreService
  ) {
    this.uVS = 0;
  }

  ngOnInit() {
    this.store.observable.subscribe((data: StoreData) => {
      this.storeData = data;
      this.uVS = this.storeData.users.map(u => u.value).reduce((a, b) => a + b);
    });
  }

}
