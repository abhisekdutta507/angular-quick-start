import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '../../modules/material/material.module';
import { LandingComponent } from './landing.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', component: LandingComponent }
];

@NgModule({
  declarations: [
    LandingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class LandingModule { }
