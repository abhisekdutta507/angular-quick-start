import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from './components.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentsComponent,
    children: [
      {
        path: 'sidenav',
        loadChildren: '../sidenav/sidenav.module#SidenavModule'
      },
      {
        path: 'dialog',
        loadChildren: '../dialog/dialog.module#DialogModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/components/sidenav',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
