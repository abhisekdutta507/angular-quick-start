import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopmotionComponent } from './popmotion.component';

describe('PopmotionComponent', () => {
  let component: PopmotionComponent;
  let fixture: ComponentFixture<PopmotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopmotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopmotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
