import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopmotionComponent } from './popmotion.component';

const routes: Routes = [
  {
    path: '',
    component: PopmotionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PopmotionRoutingModule { }
