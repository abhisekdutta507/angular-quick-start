import { Component, OnInit } from '@angular/core';
import { tween, styler, easing } from 'popmotion';

@Component({
  selector: 'app-popmotion',
  templateUrl: './popmotion.component.html',
  styleUrls: ['./popmotion.component.scss']
})
export class PopmotionComponent implements OnInit {
  private element: any;
  public showElement: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.element = styler(document.querySelector('.mat-card'));
    this.element.set({ x: 0, scale: this.showElement ? 1 : 0 });
  }

  animate() {
    tween({
      from: { x: 0, scale: this.showElement ? 1 : 0 },
      to: { x: 0, scale: !this.showElement ? 1 : 0 },
      ease: easing.cubicBezier(0.4, 0.0, 0.2, 1),
      duration: 300
    }).start({
      update: (v: any) => this.element.set(v),
      complete: () => { this.showElement = !this.showElement; }
    });
  }

}
