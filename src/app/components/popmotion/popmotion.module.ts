import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PopmotionRoutingModule } from './popmotion-routing.module';
import { PopmotionComponent } from './popmotion.component';

import { SharedModule } from '../../modules/shared/shared.module';
import { MaterialModule } from '../../modules/material/material.module';

@NgModule({
  declarations: [PopmotionComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule, ReactiveFormsModule,
    PopmotionRoutingModule
  ]
})
export class PopmotionModule { }
