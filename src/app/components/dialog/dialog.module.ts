import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../modules/shared/shared.module';
import { MaterialModule } from '../../modules/material/material.module';
import { DialogRoutingModule } from './dialog-routing.module';
import { DialogComponent, DialogModalComponent } from './dialog.component';

@NgModule({
  declarations: [
    DialogComponent,
    DialogModalComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    MaterialModule,
    DialogRoutingModule
  ],
  entryComponents: [
    DialogModalComponent
  ]
})
export class DialogModule { }
