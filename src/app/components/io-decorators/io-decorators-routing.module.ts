import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IoDecoratorsComponent } from './io-decorators.component';

const routes: Routes = [
  {
    path: '',
    component: IoDecoratorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IoDecoratorsRoutingModule { }
