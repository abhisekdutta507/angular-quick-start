import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoDecoratorsComponent } from './io-decorators.component';

describe('IoDecoratorsComponent', () => {
  let component: IoDecoratorsComponent;
  let fixture: ComponentFixture<IoDecoratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IoDecoratorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoDecoratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
