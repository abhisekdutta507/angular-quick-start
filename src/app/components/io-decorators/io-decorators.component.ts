import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { uniq, pull } from 'lodash';

import { ActionService } from '../../services/action.service';

@Component({
  selector: 'app-io-decorators',
  templateUrl: './io-decorators.component.html',
  styleUrls: ['./io-decorators.component.scss']
})
export class IoDecoratorsComponent implements OnInit {
  ioList: Array<string>;
  
  @ViewChild('addToListFormElement') addToListFormElement: ElementRef;
  
  public addToListForm = new FormGroup({
    listName: new FormControl({ value: '', disabled: false }, [])
  });

  constructor(
    private action: ActionService
  ) {
    this.ioList = ['BMW', 'Audi'];
  }

  ngOnInit() {
  }

  addToList(event: Event) {
    let v = this.addToListForm.value.listName;
    if(!v) { return; }
    this.ioList.push(v);
    this.ioList = uniq(this.ioList);
    this.clear();
  }

  async clear() {
    this.addToListForm.controls.listName.setValue('');
  }

  onRemove(item: string) {
    pull(this.ioList, item);
  }

}
