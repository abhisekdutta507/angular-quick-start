import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IoDecoratorsRoutingModule } from './io-decorators-routing.module';
import { IoDecoratorsComponent } from './io-decorators.component';

import { SharedModule } from '../../modules/shared/shared.module';
import { MaterialModule } from '../../modules/material/material.module';

@NgModule({
  declarations: [IoDecoratorsComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule, ReactiveFormsModule,
    IoDecoratorsRoutingModule
  ]
})
export class IoDecoratorsModule { }
