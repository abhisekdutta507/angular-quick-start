import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Angular8 QuickStart';
  update: boolean = false;

  constructor(public updates: SwUpdate) {
    this.updates.available.subscribe(e => { this.update = true; });
  }

  updateApplication() {
    if(environment.production) {
      this.updates.activateUpdate().then(()=>{navigator.serviceWorker.getRegistration().then((sw)=>{sw.unregister();document.location.reload();});});
    }
  }
}
